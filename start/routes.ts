/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from "@ioc:Adonis/Core/Route";

Route.get("/", async ({ response }) => {
  response.redirect().toPath("/add-participant");
});

Route.on("/add-participant").render("app.edge");
Route.get("/participants", "ParticipantsController.index");
Route.get("/participants/:id/edit", "ParticipantsController.edit");
Route.post("/participants", "ParticipantsController.store");
Route.put("/participants/:id", "ParticipantsController.update");
Route.delete("/participants/:id", "ParticipantsController.destroy").as(
  "participant.delete"
);
Route.get("/participants/:id", "ParticipantsController.show");
Route.get("/certificate/:id", "CertificatesController.certificate");
Route.get("/nametag/:id", "NametagsController.nametag");
