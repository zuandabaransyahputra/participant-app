import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Participants extends BaseSchema {
  protected tableName = "participants";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table.string("firstname").notNullable();
      table.string("lastname").notNullable();
      table.string("email").notNullable().unique();
      table.string("businessname").notNullable();
      table.string("phone").unique();
      table.timestamps(true, true);
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
