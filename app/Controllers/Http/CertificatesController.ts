import type { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Participant from "App/Models/Participant";

export default class CertificatesController {
  public async certificate({ params, view }: HttpContextContract) {
    let data = await Participant.findByOrFail("id", params.id);
    return view.render("certificate", { data });
  }
}
