import type { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Participant from "App/Models/Participant";
import ParticipantValidator from "App/Validators/ParticipantValidator";

export default class ParticipantsController {
  public async index({ view }: HttpContextContract) {
    let data = await Participant.all();
    return view.render("participants", { data });
    // response.ok({ data });
  }

  public async store({ response, request }: HttpContextContract) {
    let data = await request.validate(ParticipantValidator);
    await Participant.create(data);
    response.redirect("/participants");
  }

  public async show({ params, response }: HttpContextContract) {
    let data = await Participant.findByOrFail("id", params.id);

    response.ok({
      message: "successfully get data by id",
      data,
    });
  }

  public async update({ response, request, params }: HttpContextContract) {
    let data = await Participant.findByOrFail("id", params.id);
    data.firstname = request.input("firstname");
    data.lastname = request.input("lastname");
    data.email = request.input("email");
    data.businessname = request.input("businessname");
    data.phone = request.input("phone");
    await data.save();
    response.redirect("/participants");
  }

  public async edit({ params, view }: HttpContextContract) {
    let data = await Participant.findByOrFail("id", params.id);

    return view.render("edit", { data });
  }

  public async destroy({ response, params }: HttpContextContract) {
    let data = await Participant.findByOrFail("id", params.id);
    await data?.delete();
    return response.redirect().back();
  }
}
