import type { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Participant from "App/Models/Participant";

export default class NametagsController {
  public async nametag({ params, view }: HttpContextContract) {
    let data = await Participant.findByOrFail("id", params.id);
    return view.render("nametag", { data });
  }
}
